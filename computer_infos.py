import platform
import psutil

import json

'''
Used for get hardware informations of the computer
'''
class ComputerInfos:
    __infos = {}

    def __init__(self):
        self.__infos = ComputerInfos.all()
        pass

    def all():
        infos = {}
        infos['name'] = ComputerInfos.name()
        infos['cpu'] = ComputerInfos.cpu()
        infos['memory'] = ComputerInfos.memory()
        infos['crypto'] =  ComputerInfos.crypto()
        infos['network'] = ComputerInfos.network()
    
        return infos

    def name():
        return platform.node()

    def cpu():
        try:
            return ComputerInfos.__file_infos('/proc/cpuinfo', ':')
        except:
            return None

    def memory():
        try:
            return ComputerInfos.__file_infos('/proc/meminfo', ':')
        except:
            None

    def crypto():
        try:
            return ComputerInfos.__file_infos('/proc/crypto', ':')
        except:
            return None

    def network():
        infos = {}
        list_name = ['inet4', 'inet6', 'tcp4', 'tcp6', 'udp4', 'udp6', 'unix']

        for name in list_name:
            infos[name] = psutil.net_connections(name)

        infos['net_if_addrs'] = psutil.net_if_addrs()
        return infos
    
    def to_json(self, path):
        f = open(path, 'w')
        json.dump(self.__infos, f, indent=2)


    def __file_infos(file_path, separator):
        infos = {}
        with open(file_path, 'r') as f:
            lines = f.readlines()
            
            for index, line in enumerate(lines):
                spl = line.strip().split(separator)

                identifier = spl[0].strip('\t\n ')
                if (identifier == ' ' or identifier == ''): continue    #remove empty line
                content = spl[1].strip('\t\n ')
                
                infos[identifier] = []
                infos[identifier].append(content)
            return infos 


if __name__ == '__main__':
    infos = ComputerInfos()
    
    infos.to_json(ComputerInfos.name() + '_pcinfos.json')
    pass

