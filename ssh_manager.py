import time

from thread import Thread

class SSHManager:
    __connections = {}

    def __init__(self, host_list, username, password, config):
        for i, v in enumerate(host_list):
            self.__connections[v] = Thread(
                target=self.__connection, 
                args=((i, v), username, password))

    def start_all(self):
        for host, thread in self.__connections.items():
            thread.start()

    def drop_all(self):
        for host, thread in self.__connections.items():
            thread.kill()
            pass

    def wait_all(self):
        for i, v in self.__connections.items():
            v.join()

    def start_hosts(self, list_host):
        for i, v in enumerate(list_host):
            self.__connections[v].start()

    def drop_hosts(self, list_host):
        for i, v in enumerate(list_host):
            v.kill()

    def wait_hosts(self, list_host):
        for i, v in enumerate(list_host):
            self.__connections[v].join()

    def __connection(self, host, username, password):
        print('Thread #', str(host[0]), '(', str(host[1]), ') started')

        while True:
            continue
        
        print('Thread #', str(host[0]), '(', str(host[1]), ') finished')


if __name__ == '__main__':
    manager = SSHManager(['jolicoeur', 'data', 'scotty'], 'jeremi', '1234', None)
    manager.start_all()

    time.sleep(5)

    manager.drop_all()
    manager.wait_all()
    print('Main program end')
