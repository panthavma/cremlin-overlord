import threading
import sys 
import trace 

class Thread(threading.Thread): 
    __killed = False
    
    __lock = threading.RLock()

    def __init__(self, *args, **keywords): 
        threading.Thread.__init__(self, *args, **keywords) 

    def start(self): 
        self.__run_backup = self.run 
        self.run = self.__run       
        threading.Thread.start(self) 

    def __run(self): 
        sys.settrace(self.globaltrace) 
        self.__run_backup() 
        self.run = self.__run_backup 

    def globaltrace(self, frame, event, arg): 
        if event == 'call': 
            return self.localtrace 
        else: 
            return None

    def localtrace(self, frame, event, arg): 
        if self.__killed: 
            if event == 'line': 
                raise SystemExit() 
        return self.localtrace 

    def kill(self): 
        self.__killed = True

    def lock(self):
        self.__lock.acquire()
        pass

    def unlock(self):
        self.__lock.release()
        pass
