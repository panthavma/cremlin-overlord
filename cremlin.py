from cremi_map import CremiMap
from config import CONFIG
from client import Client
from server import Server
from thread import Thread

import getpass
import json
import lockfile
import os
import paramiko
import socket
import signal
import sys
import threading

os.nice(15)

def shutdown(lockfile='', ret_status=0):
    if os.path.exists(lockfile):
        os.remove(lockfile)
    os._exit(ret_status)

class CremlinServer(Server):

    __cremi_map = None

    __controller = None
    __controller_port = 0
    
    __hostsfile = ''
    __filelock = './server.lock'

    __username = ''
    __password = ''

    __ssh = {}

    def __init__(self, config, password): 
        Server.__init__(self, config)

        self.__cremi_map = CremiMap()

        mapfile = config['mapfile']
        self.__cremi_map.from_json(mapfile)

        self.__lockfile = config['lockfile']

        self.__username = config['username']
        self.__password = password

        self.__controller = Thread(target=self.__control_handler, args=())
        self.__controller.start()
            
    def __control_handler(self):
        while True:
            (conn, addr) = self._socket.accept()
            while True:
                data = self._recvall(conn).decode('utf-8')
                print(data)
                if len(data) > 0:
                    argv = data.split(' ')

                    # Try connect ssh of all host in argv[1:]
                    command = argv[0]
                    if command == 'stop':
                        shutdown(self.__lockfile)


                    if command == 'conn':
                        for host in argv[1:]:
                            if host in self.__ssh:
                                print('Client already connected. Do nothing.')

                            else:
                                self.__ssh[host] = {}
                                self.__ssh[host]['thread'] = Thread(target=self.__try_ssh, args=(host,))
                                self.__ssh[host]['thread'].start()
                        continue

                    if command == 'users':
                        for host in argv[1:]:
                            (i, o, e) = self.__ssh[host]['ssh'].exec_command('users')
                            print(o.read())
                        continue
                    

    def __try_ssh(self, host):
        ssh = paramiko.SSHClient()
        try:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) 
            ssh.connect(host,
                username = self.__username,
                password = self.__password)

            print(host, ' connected')
            lock = threading.RLock()

            lock.acquire()
            self.__ssh[host]['ssh'] = ssh
            lock.release()

        except paramiko.ssh_exception.AuthenticationException:
            print('Wrong password')

class CremlinClient(Client):
    def __init__(self, config):
        Client.__init__(self, config)

    def prompt(self, process_args=None):
        while True:
            input_data = input('>>')
            
            argv = input_data.split(' ')
# TODO encrypts data before sending to the server
#            if argv[0] == 'password':
#                input_data += ' ' + getpass.getpass()

            self._send(self._socket, input_data)
            if argv[0] == 'stop':
                shutdown()


class Main:
    __mode = 'client'
    __filelock = './server.lock'
    __mode_argv = {}

    def __init__(self, config):
        signal.signal(signal.SIGINT, self.__on_exit)

        self.__lockfile = config['lockfile']

        self.__mode_argv = {
            'start': self.__start,
            'reset': self.__reset,
            'exec': self.__exec,
            'users': self.__users,
            'prompt': self.__prompt,
        }
        
        if len(sys.argv) >= 2:
            argv = sys.argv[1]

            for mode, func in self.__mode_argv.items():
                if mode == argv:
                    func(sys.argv[1:])
        else:
            self.__prompt()
            sys.exit(1)

    def __start(self, argv):
        self.mode = 'server'
        
        password = getpass.getpass()
        
        if os.path.exists(self.__filelock):
            print('''
Error, server.lock is already locked.
If the server is not started, delete server.lock or use \'./Cremlin reset\' ''')
            sys.exit(1)

        cm = CremlinServer(CONFIG, password)
        cm.start()

    def __reset(self, argv):
        print('RESET')
        if os.path.exists(self.__filelock):
            os.remove(self.__filelock)
        sys.exit(0)

    def __exec(self, argv):
        self.__mode = 'client'

        if len(argv) >= 2:
            for arg in argv[1:]:
                if '--python' in arg:
                    arg = arg.strip()
                    # arg = 'python=file.py'
                    arg = arg.split('=')
                    if len(arg) == 2:   #['python', 'file.py']
                        # Open file and process it
                        pass
                    else:
                        # Case where arg contains 0 or more than 1 equal
                        print('Wrong argument, usage: --python=file.py')
                        sys.exit(1)

    def __users(self, argv):
        pass

    def __setpassword(self, argv):
        pass


    def __prompt(self, argv):
        cm = CremlinClient(CONFIG)
        cm.start(cm.prompt)
        pass

    def __on_exit(self):
        if os.path.exists(self.__filelock):
            if self.__mode == 'server':
                os.remove(self.__filelock)
        os._exit(0)
        pass

    def usage():
        print('''
            Usage: ./Cremlin MODE args ...

            Available Mode:
                - start
                    Start the server.
                - reset
                    Reset the server.
        ''')

if __name__ == '__main__':
    Main(CONFIG)
