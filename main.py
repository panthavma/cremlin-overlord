import getpass
import paramiko
import sys

argv = sys.argv
if len(argv) != 3:
    exit()

username =  argv[1]
known_host = argv[2]

password = getpass.getpass()


server = 'jaguar.emi.u-bordeaux.fr'

ssh = paramiko.SSHClient()
ssh.load_host_keys(known_host)
ssh.connect(server, 
            username=username, 
            password=password)

#ssh.exec_command('')

