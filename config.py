EXTRA_HOST = [
            'trelawney', 
            'mcgonagall', 
            'infini1', 
            'inifi2', 
            'jolicoeur', 
            'cocatris',
            'boursouf',
            'boursouflet',
            'jolicoeur',
            'tesla',
            'xeonphi'
]

CONFIG = {
        # Server config
        'host': '127.0.0.1',
        'port': 5643,
        'buffer_size': 2048,

        # SSH config
        'username': '',
            
        # Map config
        'mapfile': 'room_map.json',
}

